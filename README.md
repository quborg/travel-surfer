# TRAVEL SURFER
## this wiki is about personal development environment
## still need redaction for standard preparation

## 1. System Environment
```
npm i express -g
npm i gulp -g
```

## 2. RUN APP
```
$ gulp
```

## 3. DB
```
$ mongod -dbpath ~/MongoDB
```

## 4. MOBILE
```
$ emulator -avd <name|nexus5>
$ react-native run-<android|ios>
$ react-native start
```
